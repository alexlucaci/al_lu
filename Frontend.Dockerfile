# Build the Go Binary.
FROM golang:1.14.4 as build-frontend
ENV CGO_ENABLED 0
ARG PACKAGE_NAME
ARG PACKAGE_PREFIX

# Create a location in the container for the source code. Using the
# default GOPATH location.
RUN mkdir -p /service

# Copy the source code into the container.
WORKDIR /service
COPY . .

# Build the service binary. We are doing this last since this will be different
# every time we run through this process.
WORKDIR /service/app/frontend/
RUN go build


# Run the Go Binary in Alpine.
FROM alpine:3.7
ARG BUILD_DATE
ARG PACKAGE_NAME
ARG PACKAGE_PREFIX
COPY --from=build-frontend /service/private.pem /app/private.pem
COPY --from=build-frontend /service/app/frontend/frontend /app/main
COPY --from=build-frontend /service/frontend /app/frontend
COPY --from=build-frontend /service/startfrontend.sh /app/startfrontend.sh

WORKDIR /app

CMD sh /app/startfrontend.sh

LABEL org.opencontainers.image.created="${BUILD_DATE}" \
      org.opencontainers.image.title="${PACKAGE_NAME}" \
      org.opencontainers.image.authors="Alex Lucaci <alexlucaci@usefulpython.com>" \
      org.opencontainers.image.vendor="Alex Lucaci"


