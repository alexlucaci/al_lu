package database

import (
	"context"
	"fmt"
	_ "github.com/go-sql-driver/mysql" // The database driver in use.
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"net/url"
)

// Config has the required properties to use the database.
type Config struct {
	User     string
	Password string
	Host     string
	Name     string
}

// Open knows how to open a database connection based on the configuration.
func Open(cfg Config) (*sqlx.DB, error) {

	//Query parameters.
	q := make(url.Values)
	q.Set("parseTime", "true")

	// Construct url.
	u := url.URL{
		User:     url.UserPassword(cfg.User, cfg.Password),
		Host:     fmt.Sprintf("tcp(%s)", cfg.Host),
		Path:     cfg.Name,
		RawQuery: q.Encode(),
	}

	// Removing `//` before the url as we don't have any scheme in place for mariadb
	uri := u.String()[2:]

	db, err := sqlx.Open("mysql", uri)
	if err != nil {
		return nil, errors.Wrap(err, "cannot open sql connection")
	}
	db.SetMaxIdleConns(0)

	return db, nil
}

// StatusCheck returns nil if it can successfully talk to the database. It
// returns a non-nil error otherwise.
func StatusCheck(ctx context.Context, db *sqlx.DB) error {

	// Run a simple query to determine connectivity. The db has a "Ping" method
	// but it can false-positive when it was previously able to talk to the
	// database but the database has since gone away. Running this query forces a
	// round trip to the database.
	const q = `SELECT true`
	var tmp bool
	return db.QueryRowContext(ctx, q).Scan(&tmp)
}
