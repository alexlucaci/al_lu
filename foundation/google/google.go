package google

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Config has the required info to use the Google API.
type Config struct {
	ClientID string
	ClientSecret string
	RedirectURL string
}

func Client(cfg Config) *oauth2.Config {

	conf := oauth2.Config{
		ClientID:     cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		Endpoint:     google.Endpoint,
		RedirectURL:  cfg.RedirectURL,
		Scopes:       []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
	}

	return &conf
}
