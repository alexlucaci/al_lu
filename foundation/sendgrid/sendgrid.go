package sendgrid

import (
	"github.com/pkg/errors"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

func Client(apiKey string) *sendgrid.Client {
	return sendgrid.NewSendClient(apiKey)
}

func SendResetPasswordMail(client *sendgrid.Client, toMail, link string) error {
	from := mail.NewEmail("Alex Lucaci", "alexlucaci@usefulpython.com")
	to := mail.NewEmail(toMail, toMail)
	subject := "Reset Password Link"

	htmlContent := "<strong>You can reset your password by using the " +
		"link below. Please not that it will expire after 15 minutes</strong>" +
		"<a href=" + link + ">" + link + "</a>"

	message := mail.NewSingleEmail(from, subject, to, "Reset password link", htmlContent)
	res, err := client.Send(message)
	if err != nil {
		return errors.Wrap(err, "sending reset password email")
	}

	if res.StatusCode != 202 {
		return errors.New("send mail failed:" + res.Body)
	}

	return nil
}
