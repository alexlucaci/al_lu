package handlers

import (
	"context"
	"go-base-service/foundation/web"
	"net/http"
	"time"
)

type logoutHandlers struct{
	backendUrl string
}

func (h *logoutHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	// In order to logout, we have to delete the cookie from the browser
	// To delete a cookie, simply set the cookie again with empty value and expiry time in the past
	
	c := &http.Cookie{
		Name:     "jwt_token",
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}
	http.SetCookie(w, c)

	return web.NewRedirectError("/login", http.StatusMovedPermanently)
}
