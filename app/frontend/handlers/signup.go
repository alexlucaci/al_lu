package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"go-base-service/business/data/user"
	"go-base-service/foundation/web"
	"net/http"
)

type SignupPage struct {
	Email string
	Password string
	ConfirmPassword string
	GoogleAuthUrl string
	Error string
}

type signupHandlers struct{
	backendUrl string
}

func (h *signupHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := SignupPage{
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
		ConfirmPassword: r.PostFormValue("confirm_password"),
		GoogleAuthUrl: h.backendUrl + "/auth/google?action=signup",
	}

	nu := user.NewUser{
		Email:           page.Email,
		Password:        page.Password,
		PasswordConfirm: page.ConfirmPassword,
		IsGoogleUser: user.BoolPointer(false),
	}

	body, err := json.Marshal(&nu)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/signup.html", page)
	}

	request, err := http.NewRequest(http.MethodPost, h.backendUrl+ "/api/users", bytes.NewBuffer(body))
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/signup.html", page)
	}

	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/signup.html", page)
	}

	if res.StatusCode == 400 {

		// We only handle field validation error here as we don't have to
		// show to the user other errors from the backend
		var errorRes web.FieldValidationErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errorRes); err != nil {
			page.Error = "internal server error"
			return web.RenderTemplate(w, "frontend/pages/signup.html", page)
		}

		if errorRes.Fields == nil {
			page.Error = errorRes.Error
			return web.RenderTemplate(w, "frontend/pages/signup.html", page)
		}

		// Prepare the string error
		strErr := ""

		for _, field := range errorRes.Fields {
			strErr += field.Error
		}
		page.Error = strErr
		return web.RenderTemplate(w, "frontend/pages/signup.html", page)
	}

	if res.StatusCode != 201 {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/signup.html", page)
	}

	// set the cookies we got from backend,we are expecting the jwt_token one here
	for _, cookie := range res.Cookies() {
		http.SetCookie(w, cookie)
	}

	http.Redirect(w, r, "/edit-profile", http.StatusMovedPermanently)

	return nil
}

func (h *signupHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := SignupPage{
		GoogleAuthUrl: h.backendUrl + "/auth/google?action=signup",
	}

	return web.RenderTemplate(w, "frontend/pages/signup.html", page)
}