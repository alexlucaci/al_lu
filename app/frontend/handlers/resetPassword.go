package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"go-base-service/business/auth"
	"go-base-service/foundation/web"
	"net/http"
	"strconv"
	"time"
)

type resetpasswordHandlers struct{
	backendUrl string
}

func (h *resetpasswordHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := ForgotPasswordPage{}
	token, ok := ctx.Value(auth.KeyTokenValue).(string)
	if !ok {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	payload := struct {
		Password string `json:"password"`
		ConfirmPassword string `json:"confirm_password"`
	}{
		Password: r.PostFormValue("password"),
		ConfirmPassword: r.PostFormValue("confirm_password"),
	}

	body, err := json.Marshal(&payload)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	request, err := http.NewRequest(http.MethodPost, h.backendUrl+ "/api/reset-password?token="+token, bytes.NewBuffer(body))
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	if res.StatusCode == 404 {
		page.Error = "email not found in database"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	if res.StatusCode != 200 {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	page.Error = "password changed, please login again"
	return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
}

func (h *resetpasswordHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := ForgotPasswordPage{}

	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	//Get the token from query param
	qParams := r.URL.Query()
	tokenParams, ok := qParams["token"]
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}
	token := tokenParams[0]

	request, err := http.NewRequest(http.MethodGet, h.backendUrl+ "/api/reset-password?token="+token, nil)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	if res.StatusCode != 200 {
		page.Error = "link expired"
		return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
	}

	// Set the JWT reset password token as an http only cookie which will expire when the token also expires
	http.SetCookie(w, &http.Cookie{
		Name:       "jwt_token",
		Value:      token,
		Path:       "/",
		Expires:    time.Unix(claims.ExpiresAt, 0),
		RawExpires: strconv.FormatInt(claims.ExpiresAt, 10),
		Secure:     false,
		HttpOnly:   true,
	})

	return web.RenderTemplate(w, "frontend/pages/reset-password.html", page)
}