package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"go-base-service/business/auth"
	"go-base-service/business/data/user"
	"go-base-service/foundation/web"
	"net/http"
)

type editProfileHandlers struct{
	backendUrl string
	auth *auth.Auth
}

func (h *editProfileHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	token, ok := ctx.Value(auth.KeyTokenValue).(string)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	page := ProfilePage{
		Email:    r.PostFormValue("email"),
		PhoneNumber: r.PostFormValue("phone_number"),
		Address: r.PostFormValue("address"),
		FullName: r.PostFormValue("full_name"),
	}

	uu := user.UpdateUser{
		Email:       &page.Email,
		FullName:    &page.FullName,
		Address:     &page.Address,
		PhoneNumber: &page.PhoneNumber,
	}

	body, err := json.Marshal(&uu)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	request, err := http.NewRequest(http.MethodPut, h.backendUrl+ "/api/users/" + claims.Subject, bytes.NewBuffer(body))
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}
	request.Header.Add("Authorization", "Bearer " + token)

	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	if res.StatusCode == 400 {
		var errorRes struct{
			Error string `json:"error"`
		}
		if err := json.NewDecoder(res.Body).Decode(&errorRes); err != nil {
			page.Error = "internal server error"
			return web.RenderTemplate(w, "frontend/pages/edit-profile.html", page)
		}

		page.Error = errorRes.Error
		return web.RenderTemplate(w, "frontend/pages/edit-profile.html", page)
	}

	if res.StatusCode != 204 {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/edit-profile.html", page)
	}

	http.Redirect(w, r, "/profile", http.StatusMovedPermanently)

	return nil
}

func (h *editProfileHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	token, ok := ctx.Value(auth.KeyTokenValue).(string)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	bearer := "Bearer " + token

	request, err := http.NewRequest(http.MethodGet, h.backendUrl+ "/api/users/" + claims.Subject, nil)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	request.Header.Add("Authorization", bearer)
	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	var u user.User
	if err := json.NewDecoder(res.Body).Decode(&u); err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	var page ProfilePage
	page.Email = u.Email
	page.IsGoogleUser = u.IsGoogleUser
	if u.PhoneNumber != nil {
		page.PhoneNumber = *u.PhoneNumber
	}
	if u.Address != nil {
		page.Address = *u.Address
	}
	if u.FullName != nil {
		page.FullName = *u.FullName
	}

	return web.RenderTemplate(w, "frontend/pages/edit-profile.html", page)
}