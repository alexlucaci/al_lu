package handlers

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"go-base-service/foundation/web"
	"net/http"
	"regexp"
)

var (
	rxEmail = regexp.MustCompile(".+@.+\\..+")
	rxPassword = regexp.MustCompile("^.{4,12}$")
)

// ErrInvalidEmail occurs when a user submits login data with
// an email that is invalid
var ErrEmailValidation = errors.New("email entered is not a valid email")

// ErrInvalidEmail occurs when a user submits login data with
// a password that is invalid (not between 4 and 12 characters)
var ErrPasswordValidation = errors.New("password must be between 4 and 12 chars")


type LoginPage struct {
	Email string
	Password string
	GoogleAuthUrl string
	Error string
}

// Validate will validate the login payload fields
// Email should be a valid email address
// Password should be between 4 and 12 chars
func (p *LoginPage) Validate() error {
	if match := rxEmail.Match([]byte(p.Email)); match == false {
		return ErrEmailValidation
	}

	if match := rxPassword.Match([]byte(p.Password)); match == false {
		return ErrPasswordValidation
	}

	return nil
}

type loginHandlers struct{
	backendUrl string
}

func (h *loginHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := LoginPage{
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
		GoogleAuthUrl: h.backendUrl + "/auth/google?action=login",
	}

	if err := page.Validate(); err != nil {
		page.Error = err.Error()
		return web.RenderTemplate(w, "frontend/pages/login.html", page)
	}

	request, err := http.NewRequest(http.MethodGet, h.backendUrl+ "/api/login", nil)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/login.html", page)
	}

	request.SetBasicAuth(page.Email, page.Password)
	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/login.html", page)
	}

	if res.StatusCode == 401 {
		var errorRes struct{
			Error string `json:"error"`
		}
		if err := json.NewDecoder(res.Body).Decode(&errorRes); err != nil {
			page.Error = "internal server error"
			return web.RenderTemplate(w, "frontend/pages/login.html", page)
		}

		page.Error = errorRes.Error
		return web.RenderTemplate(w, "frontend/pages/login.html", page)
	}

	if res.StatusCode != 200 {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/login.html", page)
	}

	// set the cookies we got from backend,we are expecting the jwt_token one here
	for _, cookie := range res.Cookies() {
		http.SetCookie(w, cookie)
	}

	http.Redirect(w, r, "/profile", http.StatusMovedPermanently)

	return nil
}

func (h *loginHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

	return web.RenderTemplate(w, "frontend/pages/login.html", LoginPage{GoogleAuthUrl: h.backendUrl + "/auth/google?action=login"})
}