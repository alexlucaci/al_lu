package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"go-base-service/foundation/web"
	"net/http"
)

type ForgotPasswordPage struct{
	Error string
}

type forgotpasswordHandlers struct{
	backendUrl string
}

func (h *forgotpasswordHandlers) post(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := ForgotPasswordPage{}
	payload := struct {
		Email string
	}{
		Email: r.PostFormValue("email"),
	}

	body, err := json.Marshal(&payload)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
	}

	request, err := http.NewRequest(http.MethodPost, h.backendUrl+ "/api/forgot-password", bytes.NewBuffer(body))
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
	}

	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
	}

	if res.StatusCode == 404 {
		page.Error = "email not found in database"
		return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
	}

	if res.StatusCode != 200 {
		page.Error = "internal server error"
		return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
	}

	page.Error = "an email with a reset password link was sent to your email"

	return web.RenderTemplate(w, "frontend/pages/forgot-password.html", page)
}

func (h *forgotpasswordHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	return web.RenderTemplate(w, "frontend/pages/forgot-password.html", nil)
}