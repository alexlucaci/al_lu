package handlers

import (
	"context"
	"go-base-service/business/auth"
	"go-base-service/business/mid"
	"go-base-service/foundation/web"
	"log"
	"net/http"
	"os"
)

// Frontend constructs an http.Handler with all the frontend routes defined.
func Frontend(shutdown chan os.Signal, log *log.Logger, a *auth.Auth, backendUrl string) http.Handler {
	app := web.NewApp(shutdown, mid.Logger(log), mid.Errors(log), mid.Panics(log))

	// register the static file server
	app.Handle(http.MethodGet, "/static/*", fileServer)

	lh := loginHandlers{backendUrl}

	app.Handle(http.MethodPost, "/login", lh.post)
	app.Handle(http.MethodGet, "/login", lh.get)

	lgh := logoutHandlers{backendUrl}

	app.Handle(http.MethodPost, "/logout", lgh.post, mid.AuthenticateFromCookie(a))

	sh := signupHandlers{backendUrl: backendUrl}

	app.Handle(http.MethodPost, "/signup", sh.post)
	app.Handle(http.MethodGet, "/signup", sh.get)

	ph := editProfileHandlers{
		backendUrl: backendUrl,
		auth: a,
	}

	app.Handle(http.MethodPost, "/edit-profile", ph.post, mid.AuthenticateFromCookie(a))
	app.Handle(http.MethodGet, "/edit-profile", ph.get, mid.AuthenticateFromCookie(a))

	mph := profileHandlers{
		backendUrl: backendUrl,
		auth: a,
	}

	app.Handle(http.MethodGet, "/profile", mph.get, mid.AuthenticateFromCookie(a))

	fh := forgotpasswordHandlers{
		backendUrl: backendUrl,
	}

	app.Handle(http.MethodPost, "/forgot-password", fh.post)
	app.Handle(http.MethodGet, "/forgot-password", fh.get)

	rh := resetpasswordHandlers{
		backendUrl: backendUrl,
	}

	app.Handle(http.MethodPost, "/reset-password", rh.post, mid.AuthenticateFromCookieResetPassword(a))
	app.Handle(http.MethodGet, "/reset-password", rh.get, mid.ValidatePasswordResetToken(a))

	return app
}

func fileServer(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	fs := http.FileServer(http.Dir("./frontend/static"))
	fs = http.StripPrefix("/static", fs)
	fs.ServeHTTP(w, r)
	return nil
}
