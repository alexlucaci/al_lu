package handlers

import (
	"context"
	"encoding/json"
	"go-base-service/business/auth"
	"go-base-service/business/data/user"
	"go-base-service/foundation/web"
	"net/http"
)

type ProfilePage struct {
	Email string
	Address string
	FullName string
	PhoneNumber string
	IsGoogleUser bool
	Error string
}

type profileHandlers struct{
	backendUrl string
	auth *auth.Auth
}

func (h *profileHandlers) get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	token, ok := ctx.Value(auth.KeyTokenValue).(string)
	if !ok {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	bearer := "Bearer " + token

	request, err := http.NewRequest(http.MethodGet, h.backendUrl+ "/api/users/" + claims.Subject, nil)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	request.Header.Add("Authorization", bearer)
	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	var u user.User
	if err := json.NewDecoder(res.Body).Decode(&u); err != nil {
		return web.NewRedirectError("/login", http.StatusMovedPermanently)
	}

	var page ProfilePage
	page.Email = u.Email
	if u.PhoneNumber != nil {
		page.PhoneNumber = *u.PhoneNumber
	}
	if u.Address != nil {
		page.Address = *u.Address
	}
	if u.FullName != nil {
		page.FullName = *u.FullName
	}

	return web.RenderTemplate(w, "frontend/pages/profile.html", page)
}