package handlers

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"go-base-service/business/auth"
	"go-base-service/business/data/user"
	"go-base-service/foundation/web"
	"golang.org/x/oauth2"
	"net/http"
	"strconv"
	"time"
)

type googleAuthHandlers struct {
	db   *sqlx.DB
	auth *auth.Auth
	frontendUrl  string
	googleConfig *oauth2.Config
}

func (h *googleAuthHandlers) google(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	qParams := r.URL.Query()
	actionParams, ok := qParams["action"]
	if !ok {
		return web.NewRedirectError(h.frontendUrl+"/login", http.StatusMovedPermanently)
	}
	action := actionParams[0]

	// if action is not either login or signup then someone tampered the url so redirect to login
	// otherwise put it in a cookie to handle the callback logic
	if action != "login" && action != "signup" {
		return web.NewRedirectError(h.frontendUrl+"/login", http.StatusMovedPermanently)
	}

	c := http.Cookie{
		Name:       "action",
		Value:      action,
		Path:       "/",
		Expires:    time.Now().Add(15 * time.Second),
		HttpOnly:   true,
	}
	http.SetCookie(w, &c)

	// create random state
	b := make([]byte, 32)
	if _, err := rand.Read(b); err != nil {
		return web.NewRedirectError(h.frontendUrl+"/login", http.StatusMovedPermanently)
	}

	state := base64.StdEncoding.EncodeToString(b)

	// put the state as an httpOnly cookie with expiration in 15seconds
	c = http.Cookie{
		Name:       "state",
		Value:      state,
		Path:       "/",
		Expires:    time.Now().Add(15 * time.Second),
		HttpOnly:   true,
	}
	http.SetCookie(w, &c)

	// and redirect to the google oauth url
	url := h.googleConfig.AuthCodeURL(state)
	http.Redirect(w, r, url, http.StatusMovedPermanently)

	return nil
}

func (h *googleAuthHandlers) googleCallback(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	// Search the cookies for the action
	ac, err := r.Cookie("action")
	if err != nil {
		return web.NewRedirectError(h.frontendUrl+"/login", http.StatusMovedPermanently)
	}
	action := ac.Value

	qParams := r.URL.Query()

	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// get the state from cookie
	state, err := r.Cookie("state")
	if err != nil {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// get the state from the googleCallback url
	callbackState, ok := qParams["state"]
	if !ok {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// and check if the state from cookie matches the state from the googleCallback
	if state.Value != callbackState[0] {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	code, ok := qParams["code"]
	if !ok {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// now use the googleCallback code to get the token from oauth exchange
	tkn, err := h.googleConfig.Exchange(ctx, code[0])
	if err != nil  {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// construct the oauth client to retrieve the user info
	client := h.googleConfig.Client(ctx, tkn)

	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	var u struct {
		Email string `json:"email"`
		Name string `json:"name"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&u); err != nil {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// Try to create the users and:
	// If the user already exists, then continue
	// If the user was created, update its details from google api
	if action == "signup" {
		nu := user.NewUser{
			Email:           u.Email,
			IsGoogleUser:    user.BoolPointer(true),
		}
		usr, err := user.Create(ctx, h.db, nu, rvs.Now)
		switch err {
		case user.ErrEmailAlreadyExists:
		case nil:
			uu := user.UpdateUser{
				FullName: &u.Name,
			}
			if err := user.Update(ctx, h.db, usr.ID, uu, rvs.Now); err != nil {
				return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
			}
		default:
			return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
		}
	}

	// Authenticate the user and generate claims
	claims, err := user.Authenticate(ctx, h.db, rvs.Now, u.Email, "", true)
	if err != nil {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// Generate JWT token
	token, err := h.auth.GenerateToken(claims)
	if err != nil {
		return web.NewRedirectError(h.frontendUrl+"/"+action, http.StatusMovedPermanently)
	}

	// Set the JWT token as an http only cookie which will expire when the token also expires
	http.SetCookie(w, &http.Cookie{
		Name:       "jwt_token",
		Value:      token,
		Path:       "/",
		Expires:    time.Unix(claims.ExpiresAt, 0),
		RawExpires: strconv.FormatInt(claims.ExpiresAt, 10),
		Secure:     false,
		HttpOnly:   true,
	})

	// Remove the state and action cookies
	http.SetCookie(w, &http.Cookie{
		Name:     "state",
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	})
	http.SetCookie(w, &http.Cookie{
		Name:     "action",
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	})

	return web.NewRedirectError(h.frontendUrl+"/profile", http.StatusMovedPermanently)
}
