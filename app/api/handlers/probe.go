package handlers

import (
	"context"
	"go-base-service/foundation/web"
	"net/http"
)

type probe struct{}

// ready handler defined the readiness probe and will return 200 status code
func (h *probe) ready(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	return web.Respond(ctx, w, nil, http.StatusOK)
}
