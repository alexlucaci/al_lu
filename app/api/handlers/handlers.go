// handlers package containers the handler functions and routes supported by the api
package handlers

import (
	"github.com/jmoiron/sqlx"
	"github.com/sendgrid/sendgrid-go"
	"go-base-service/business/auth"
	"go-base-service/business/mid"
	"go-base-service/foundation/web"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"os"
)

// API constructs an http.Handler with all app routes defined.
func API(shutdown chan os.Signal, log *log.Logger, db *sqlx.DB, a *auth.Auth, g *oauth2.Config, sg *sendgrid.Client, frontendUrl string) http.Handler {

	app := web.NewApp(shutdown, mid.Logger(log), mid.Errors(log), mid.Panics(log))

	// Register the probe endpoints. Route not authenticated.
	p := probe{}

	app.Handle(http.MethodGet, "/api/health/ready", p.ready)

	gaH := googleAuthHandlers{
		db:   db,
		auth: a,
		frontendUrl: frontendUrl,
		googleConfig: g,
	}

	app.Handle(http.MethodGet, "/auth/google", gaH.google)
	app.Handle(http.MethodGet, "/auth/google/callback", gaH.googleCallback)

	// Register user management endpoints.
	u := userHandlers{
		db:   db,
		auth: a,
	}

	app.Handle(http.MethodGet, "/api/users/:id", u.retrieve, mid.AuthenticateFromBearer(a))
	app.Handle(http.MethodPut, "/api/users/:id", u.update, mid.AuthenticateFromBearer(a))

	// These routes are not authenticated
	app.Handle(http.MethodGet, "/api/login", u.login)

	app.Handle(http.MethodPost, "/api/users", u.create)

	ph := passwordHandlers{
		db:   db,
		auth: a,
		sg: sg,
		frontendUrl: frontendUrl,
	}
	app.Handle(http.MethodPost,"/api/forgot-password", ph.forgot)
	app.Handle(http.MethodGet,"/api/reset-password", ph.validate, mid.ValidatePasswordResetToken(a))
	app.Handle(http.MethodPost, "/api/reset-password", ph.reset, mid.ValidatePasswordResetToken(a))

	return app
}
