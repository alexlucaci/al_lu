package handlers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/sendgrid/sendgrid-go"
	"go-base-service/business/auth"
	"go-base-service/business/data/user"
	mailClient "go-base-service/foundation/sendgrid"
	"go-base-service/foundation/web"
	"golang.org/x/net/context"
	"net/http"
	"time"
)

type passwordHandlers struct {
	db   *sqlx.DB
	auth *auth.Auth
	sg *sendgrid.Client
	frontendUrl string
}

func (h *passwordHandlers) forgot(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewShutdownError("request values missing from context")
	}

	// We are expecting an email so try to unmarshall it
	var res struct {Email string `json:"email" validate:"required"`}
	if err := web.Decode(r, &res); err != nil {
		return errors.Wrap(err, "decoding forgot password payload")
	}

	// Check if the email exists in db
	// Also we don't have to change password for google account so filter them out
	// we will display email not found if the email requested to change password from
	// is from google auth
	u, err := user.OneByEmailAndIsGoogle(ctx, h.db, res.Email, false)
	if err != nil {
		return web.NewGenericError(err, http.StatusNotFound)
	}

	// now create claims, but change the Audience to "reset-password" to differentiate
	// between auth claims and reset password claims
	claims := auth.Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "auth-backend",
			Subject:   u.ID,
			Audience:  "reset-password",
			ExpiresAt: rvs.Now.Add(time.Minute * 15).Unix(),
			IssuedAt:  rvs.Now.Unix(),
		},
	}

	// Create the token to use in the reset password link
	token, err := h.auth.GenerateToken(claims)
	if err != nil {
		return errors.Wrap(err, "generating token for reset password link")
	}

	link := h.frontendUrl + "/reset-password?token="+token

	if err := mailClient.SendResetPasswordMail(h.sg, u.Email, link); err != nil {
		return errors.Wrapf(err, "sending email")
	}

	return web.Respond(ctx, w, struct{}{}, 200)
}

func (h *passwordHandlers) validate(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	return web.Respond(ctx, w, struct{}{}, 200)
}

func (h *passwordHandlers) reset(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewShutdownError("request values missing from context")
	}

	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return errors.New("auth claims missing from context")
	}

	// We are expecting a password so try to unmarshall
	var res struct {
		Password        string `json:"password" validate:"required"`
		PasswordConfirm string `json:"confirm_password" validate:"required,eqfield=Password"`
	}
	if err := web.Decode(r, &res); err != nil {
		return errors.Wrap(err, "decoding reset password payload")
	}

	// Update the user password
	if err := user.ChangePassword(ctx, h.db, claims.Subject, res.Password, rvs.Now); err != nil {
		return errors.Wrap(err, "changing user password")
	}

	return web.Respond(ctx, w, struct{}{}, 200)
}
