package handlers

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go-base-service/business/auth"
	"go-base-service/business/data/user"
	"go-base-service/foundation/web"
	"net/http"
	"strconv"
	"time"
)

// ErrForbidden occurs when a user tries to do something that is forbidden
// to them according to our access control policies.
var ErrForbidden = errors.New("attempted action is not allowed")

type userHandlers struct {
	db   *sqlx.DB
	auth *auth.Auth
}

func (h *userHandlers) retrieve(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return errors.New("claims missing from context")
	}

	params := web.Params(r)

	// If someone is trying to retrieve another user that himself
	if claims.Subject != params["id"] {
		return web.NewGenericError(ErrForbidden, http.StatusForbidden)
	}

	usr, err := user.One(ctx, h.db, params["id"])
	if err != nil {
		switch err {
		case user.ErrInvalidID:
			return web.NewGenericError(err, http.StatusBadRequest)
		case user.ErrNotFound:
			return web.NewGenericError(err, http.StatusNotFound)
		default:
			return errors.Wrapf(err, "Id: %s", params["id"])
		}
	}

	return web.Respond(ctx, w, usr, http.StatusOK)
}

func (h *userHandlers) create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewShutdownError("request values missing from context")
	}

	// First decode the payload data
	var nu user.NewUser
	if err := web.Decode(r, &nu); err != nil {
		return errors.Wrap(err, "decoding user payload")
	}

	// Then create the user
	usr, err := user.Create(ctx, h.db, nu, rvs.Now)
	if err != nil {
		switch err {
		case user.ErrInvalidID:
			return web.NewGenericError(err, http.StatusBadRequest)
		case user.ErrNotFound:
			return web.NewGenericError(err, http.StatusNotFound)
		case user.ErrEmailAlreadyExists:
			return web.NewGenericError(err, http.StatusBadRequest)
		default:
			return web.NewGenericError(nil, http.StatusBadRequest)
		}
	}

	// Authenticate the user and generate claims
	claims, err := user.Authenticate(ctx, h.db, rvs.Now, nu.Email, nu.Password, false)
	if err != nil {
		switch err {
		case user.ErrAuthenticationFailure:
			return web.NewGenericError(err, http.StatusUnauthorized)
		default:
			return errors.Wrap(err, "authenticating")
		}
	}

	// Generate JWT token
	token, err := h.auth.GenerateToken(claims)
	if err != nil {
		return errors.Wrap(err, "generating access token")
	}

	// Set the JWT token as an http only cookie which will expire when the token also expires
	http.SetCookie(w, &http.Cookie{
		Name:       "jwt_token",
		Value:      token,
		Path:       "/",
		Expires:    time.Unix(claims.ExpiresAt, 0),
		RawExpires: strconv.FormatInt(claims.ExpiresAt, 10),
		Secure:     false,
		HttpOnly:   true,
	})

	return web.Respond(ctx, w, usr, http.StatusCreated)
}

func (h *userHandlers) update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewShutdownError("request values missing from context")
	}

	claims, ok := ctx.Value(auth.KeyClaimsValue).(auth.Claims)
	if !ok {
		return errors.New("claims missing from context")
	}

	var uu user.UpdateUser
	if err := web.Decode(r, &uu); err != nil {
		return errors.Wrap(err, "")
	}

	params := web.Params(r)

	// If someone is trying to retrieve another user that himself
	if claims.Subject != params["id"] {
		return web.NewGenericError(ErrForbidden, http.StatusForbidden)
	}

	err := user.Update(ctx, h.db, params["id"], uu, rvs.Now)
	if err != nil {
		switch err {
		case user.ErrInvalidID:
			return web.NewGenericError(err, http.StatusBadRequest)
		case user.ErrNotFound:
			return web.NewGenericError(err, http.StatusNotFound)
		case user.ErrEmailAlreadyExists:
			return web.NewGenericError(err, http.StatusBadRequest)
		default:
			return web.NewGenericError(nil, http.StatusBadRequest)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h *userHandlers) login(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	rvs, ok := ctx.Value(web.KeyRequestValues).(*web.RequestValues)
	if !ok {
		return web.NewShutdownError("request values missing from context")
	}

	email, pwd, ok := r.BasicAuth()
	if !ok {
		err := errors.New("must provide email and password in Basic google")
		return web.NewGenericError(err, http.StatusUnauthorized)
	}

	// Authenticate the user and generate claims
	claims, err := user.Authenticate(ctx, h.db, rvs.Now, email, pwd, false)
	if err != nil {
		return web.NewGenericError(err, http.StatusUnauthorized)
	}

	// Generate JWT token
	token, err := h.auth.GenerateToken(claims)
	if err != nil {
		return errors.Wrap(err, "generating access token")
	}

	// Set the JWT token as an http only cookie which will expire when the token also expires
	http.SetCookie(w, &http.Cookie{
		Name:       "jwt_token",
		Value:      token,
		Path:       "/",
		Expires:    time.Unix(claims.ExpiresAt, 0),
		RawExpires: strconv.FormatInt(claims.ExpiresAt, 10),
		Secure:     false,
		HttpOnly:   true,
	})

	return web.Respond(ctx, w, struct{}{}, http.StatusOK)
}
