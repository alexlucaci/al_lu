package auth_test

import (
	"crypto/rsa"
	"errors"
	"go-base-service/business/auth"
	"go-base-service/business/tests"
	"testing"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func TestAuth(t *testing.T) {
	t.Log("Given the need to be able to authenticate and authorize access.")
	{
		t.Logf("\tWhen handling a single user.")
		{
			privateKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateRSAKey))
			if err != nil {
				t.Fatalf("\t%s\tShould be able to parse the private key from pem: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tShould be able to parse the private key from pem.", tests.Success)

			// The key id we are stating represents the public key in the
			// public key store.
			const keyID = "c9b06d61-801a-4296-82c5-3573587fcf21"

			keyLookupFunc := func(publicKID string) (*rsa.PublicKey, error) {
				if publicKID != keyID {
					return nil, errors.New("no public key found")
				}
				return &privateKey.PublicKey, nil
			}

			a, err := auth.New(privateKey, keyID, "RS256", keyLookupFunc)
			if err != nil {
				t.Fatalf("\t%s\tShould be able to create an authenticator: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tShould be able to create an authenticator.", tests.Success)

			claims := auth.Claims{
				StandardClaims: jwt.StandardClaims{
					Issuer:    "auth task",
					Subject:   "8c9eabbf-c06b-420b-8cbc-73beb1f440f1",
					Audience:  "go-frontend",
					ExpiresAt: time.Now().Add(8760 * time.Hour).Unix(),
					IssuedAt:  time.Now().Unix(),
				},
			}

			token, err := a.GenerateToken(claims)
			if err != nil {
				t.Fatalf("\t%s\tShould be able to generate a JWT: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tShould be able to generate a JWT.", tests.Success)

			_, err = a.ValidateToken(token)
			if err != nil {
				t.Fatalf("\t%s\tShould be able to parse the claims: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tShould be able to parse the claims.", tests.Success)
		}
	}
}

// Output of:
// openssl genpkey -algorithm RSA -out private.pem -pkeyopt rsa_keygen_bits:2048
// ./sales-admin keygen
const privateRSAKey = `-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDcVDeUIxgQ+ceE
G65poH52snwDnIpTBcWXQ1GZnDAEfOpvoY1dtzY7O9DjuymFKhNTt1huBovuivsW
/hZ8Od+9+/r+yd58Q8xLcJgv4Op5BQMvkrUzHjv7C6mLuRZuBti/ROIz52dRqe+7
AkTpwRYhREg226tCh5YQsFLgGQ19x7U9nPKpkfuWbkW8nvmBdMZBwvFLiaJ6gsDr
eRMBEdPXGCtBTTKeM+Wcwr8K6Q/VbLsqqMO7IBOvWCMlleV7I9W52H1ZNnM8W/tl
FsZwYsgtQi225V/CpQhmY3nG2WJNxPtttOJ2uGvxAauGGZZzY+8XViMkMefKxTaf
l4NWc9MlAgMBAAECggEAC66/f7BYr1W/OYcKOXmUGxEduEdNoldVAc9E/WMlvKcu
+xpS15xGWN+0FM8wxWnUuoSnQLZCJzThWXfBk3ayIW3CAk6L6ITjyUBAIZHIfQBC
Cfy6dOpufcJBjDIVOWJNjd7GK8BiXxcmqgDVzJs7ZCotthLxwsZTs7DVxCIv7/JU
vzj3qfEBhOeKjF01Bng7OywhQk1pozGDK5yySMYBpDsLjIJaqa4zhTjBypi5HMMQ
9qHNUQ7D8NM2xXNmuCcvqv7c2JjWLfcMwmO4RJDjn5ROkuvUXPxy1HHy1AOh6/Zt
YZoPrvrJkEVfYYdTcsisU6Sw859BbME5xp4ZYxPcpQKBgQDt5jvX1M0V7nWJnFFE
Y5bN044MsO9sOutlBHijzDcvT6STwUa0zyyYrlsBIIa78nUdgOIBWR8pSPCQMmUw
0+BxOB6u1AEbUZJhLjAMd/IoSbQFLSlYQFa0pyYPyqH6NrE9Bf3Kn3gKsDcUz8+u
6EmJKl4zZRiD/g2Z0UsHhhtePwKBgQDtF8ASlj+Yh/6rMHSAnUuvisN8CSFTTZPL
y+a7RaETmsaiEU7SYNRhYU7880KZBq7/l3M47Ttosu9zE3HRDH3TSqfqs6SsgPtZ
bU6y4I9Bx2s6WWC7fwE1jPC0Ue7rxD9UnT1xyalkV8fje4W5to8U8KaP4UQwKCqS
Iqi3i+N9mwKBgCvSRSO0pzJrAmuex7l5PsYbOgdMcE3XbJTnBfHmJCQq3334FDYq
E2TseQb0Ht1wA/KzUu48pGjObsGhBtK/tJAmbkB1/N4zYGtWPrONRKX3LZnmf78p
2s7cGz69bKjSekNrXxx703jkjMDgprjpQOJrU37UGHf3zDmRO+je4xuxAoGBAOLw
Gs8vtYvAkEk3xivdY7aQ8fBlpNphpRgrAZ07VqS3P22dd37zZBYZ2EtSZOqodRT8
2DMjD/x1WbEiaY0Zkc6O5Q9ICFV92BiPAgdB3p2tzbs6FdciB/xD/Zn72t8dJylM
oKWry1Ham9/oOeW7ElWOyz9o9aXKrITYW/nkEkoFAoGAbTSATttOVFOL94mhb2S3
W5ovHhjv2kyXgQhF5IIQwHJZoPWxKZt4GSm1Ei+n8vPZfcPDfx/MpVK+uevKRCc3
hZdt1N9YnPhgN5XXWyatswaUoAE+kP/5bM2lPY7ZylpqthUbCHZC3ZWoliThou8f
inA0PuD+hfi7fFfyOz3G9xI=
-----END PRIVATE KEY-----`
