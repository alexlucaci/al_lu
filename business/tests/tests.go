package tests

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go-base-service/business/auth"
	"go-base-service/business/data/schema"
	"go-base-service/business/data/user"
	"go-base-service/foundation/database"
	"go-base-service/foundation/web"
	"log"
	"os"
	"testing"
	"time"
)

// Success and failure markers.
const (
	Success = "\u2713"
	Failed  = "\u2717"
)

// NewUnit creates a test database inside a Docker container.
// It applies the migrations and inserts the seed data. It returns
// the database to use as well cleanup function to call at the end of the test.
func NewUnit(t *testing.T) (*sqlx.DB, func()) {
	dbC, err := database.StartContainer()
	if err != nil {
		t.Fatalf("creating database: %v", err)
	}
	t.Logf("DB ContainerID: %s", dbC.ID)
	t.Logf("DB Host: %s", dbC.Host)

	db, err := database.Open(database.Config{
		User:     "root",
		Password: "mysql",
		Host:     dbC.Host,
		Name:     "mysql_db",
	})
	if err != nil {
		t.Fatalf("opening database connection: %v", err)
	}

	t.Log("waiting for database to be ready ...")

	// Wait for the database to be ready. Wait 100ms longer between each attempt.
	// Do not try more than 20 times.
	var pingError error
	maxAttempts := 20
	for attempts := 1; attempts <= maxAttempts; attempts++ {
		pingError = db.Ping()
		if pingError == nil {
			break
		}
		time.Sleep(time.Duration(attempts) * 100 * time.Millisecond)
	}

	if pingError != nil {
		out, err := database.DumpContainerLogs(dbC)
		if err != nil {
			t.Logf("dumping container logs: %v", err)
		}
		if err := database.StopContainer(dbC); err != nil {
			t.Errorf("stopping container: %v", err)
		}
		t.Logf(out)
		t.Fatalf("database never ready: %v", pingError)
	}

	if err := schema.Migrate(db); err != nil {
		if err := database.StopContainer(dbC); err != nil {
			t.Errorf("stopping container: %v", err)
		}
		t.Fatalf("migrating error: %s", err)
	}

	// teardown is the function that should be invoked when the caller is done
	// with the database.
	teardown := func() {
		t.Helper()
		db.Close()
		if err := database.StopContainer(dbC); err != nil {
			t.Errorf("stopping db container with id %s, err:: %v", dbC.ID, err)
			return
		}
		t.Log("Stopped db container with id:", dbC.ID)
		t.Log("Removed db container with id:", dbC.ID)
	}

	return db, teardown
}

// Test owns state for running and shutting down tests.
type Test struct {
	DB  *sqlx.DB
	Log *log.Logger
	Auth *auth.Auth

	t       *testing.T
	cleanup func()
}

// NewIntegration creates a database, seeds it, constructs an authenticator.
func NewIntegration(t *testing.T) *Test {

	// Initialize and seed database. Store the cleanup function call later.
	db, cleanup := NewUnit(t)

	if err := schema.Seed(db); err != nil {
		t.Fatal(err)
	}

	log := log.New(os.Stdout, "TEST : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	// Create RSA keys to enable authentication in our service.
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Fatal(err)
	}

	// Build an authenticator using this key lookup function to retrieve
	// the corresponding public key.
	KID := "4754d86b-7a6d-4df5-9c65-224741361492"
	keyLookupFunc := func(kid string) (*rsa.PublicKey, error) {
		if kid != KID {
			return nil, errors.New("no public key found")
		}
		return privateKey.Public().(*rsa.PublicKey), nil
	}
	a, err := auth.New(privateKey, KID, "RS256", keyLookupFunc)
	if err != nil {
		t.Fatal(err)
	}

	test := Test{
		DB:      db,
		Log:     log,
		Auth:    a,
		t:       t,
		cleanup: cleanup,
	}

	return &test
}

// Teardown releases any resources used for the test.
func (test *Test) Teardown() {
	test.cleanup()
}

// Token generates an authenticated token for a user.
func (test *Test) Token(email, pass string) string {
	claims, err := user.Authenticate(context.Background(), test.DB, time.Now(), email, pass, false)
	if err != nil {
		test.t.Fatal(err)
	}

	token, err := test.Auth.GenerateToken(claims)
	if err != nil {
		test.t.Fatal(err)
	}

	return token
}

// Context returns an app level context for testing.
func Context() context.Context {
	values := web.RequestValues{
		TraceID: uuid.New().String(),
		Now:     time.Now(),
	}

	return context.WithValue(context.Background(), web.KeyRequestValues, &values)
}

// StringPointer is a helper to get a *string from a string. It is in the tests
// package because we normally don't want to deal with pointers to basic types
// but it's useful in some tests.
func StringPointer(s string) *string {
	return &s
}

// IntPointer is a helper to get a *int from a int. It is in the tests package
// because we normally don't want to deal with pointers to basic types but it's
// useful in some tests.
func IntPointer(i int) *int {
	return &i
}
