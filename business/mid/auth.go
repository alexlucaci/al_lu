package mid

import (
	"context"
	"github.com/pkg/errors"
	"go-base-service/business/auth"
	"go-base-service/foundation/web"
	"net/http"
	"strings"
)

// AuthenticateFromBearer validates a JWT from the `Authorization` header.
func AuthenticateFromBearer(a *auth.Auth) web.Middleware {

	// This is the actual middleware function to be executed.
	m := func(after web.Handler) web.Handler {

		// Create the handler that will be attached in the middleware chain.
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			// Parse the authorization header. Expected header is of
			// the format `Bearer <token>`.
			parts := strings.Split(r.Header.Get("Authorization"), " ")
			if len(parts) != 2 || strings.ToLower(parts[0]) != "bearer" {
				err := errors.New("expected authorization header format: Bearer <token>")
				return web.NewGenericError(err, http.StatusUnauthorized)
			}

			// Start a span to measure just the time spent in ParseClaims.
			claims, err := a.ValidateToken(parts[1])
			if err != nil {
				return web.NewGenericError(err, http.StatusUnauthorized)
			}

			// Make sure that the token is for api bearer purposes
			if claims.Audience != "go-frontend" {
				return web.NewGenericError(errors.New("audience invalid"), http.StatusUnauthorized)
			}

			// Add claims to the context so they can be retrieved later.
			ctx = context.WithValue(ctx, auth.KeyClaimsValue, claims)

			return after(ctx, w, r)
		}

		return h
	}

	return m
}

// ValidatePasswordResetToken will validate if a password reset link is valid
func ValidatePasswordResetToken(a *auth.Auth) web.Middleware {

	// This is the actual middleware function to be executed.
	m := func(after web.Handler) web.Handler {

		// Create the handler that will be attached in the middleware chain.
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			//Get the token from query param
			qParams := r.URL.Query()
			tokenParams, ok := qParams["token"]
			if !ok {
				return web.NewGenericError(errors.New("token not found in url"), http.StatusUnauthorized)
			}
			token := tokenParams[0]

			// Start a span to measure just the time spent in ParseClaims.
			claims, err := a.ValidateToken(token)
			if err != nil {
				return web.NewGenericError(err, http.StatusUnauthorized)
			}

			// Make sure that the token is for resetting password purposes
			if claims.Audience != "reset-password" {
				return web.NewGenericError(errors.New("audience invalid"), http.StatusUnauthorized)
			}

			// Add claims to the context so they can be retrieved later.
			ctx = context.WithValue(ctx, auth.KeyClaimsValue, claims)

			return after(ctx, w, r)
		}

		return h
	}

	return m
}

// AuthenticateFromCookie checks if a jwt token is present in the cookies and add it to the context.
// Otherwise it redirects to the login page
func AuthenticateFromCookie(a *auth.Auth) web.Middleware {

	// This is the actual middleware function to be executed.
	m := func(after web.Handler) web.Handler {

		// Create the handler that will be attached in the middleware chain.
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			// Search the cookies for the token, present with jwt_token as the key
			token, err := r.Cookie("jwt_token")
			if err != nil {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			claims, err := a.ValidateToken(token.Value)
			if err != nil {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			// Make sure that the token is for resetting password purposes
			if claims.Audience != "go-frontend" {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			// Add claims & token to the context so they can be retrieved later.
			ctx = context.WithValue(ctx, auth.KeyClaimsValue, claims)
			ctx = context.WithValue(ctx, auth.KeyTokenValue, token.Value)

			return after(ctx, w, r)
		}

		return h
	}

	return m
}

// AuthenticateFromCookie checks if a jwt token is present in the cookies and add it to the context.
// Otherwise it redirects to the login page
func AuthenticateFromCookieResetPassword(a *auth.Auth) web.Middleware {

	// This is the actual middleware function to be executed.
	m := func(after web.Handler) web.Handler {

		// Create the handler that will be attached in the middleware chain.
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			// Search the cookies for the token, present with jwt_token as the key
			token, err := r.Cookie("jwt_token")
			if err != nil {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			claims, err := a.ValidateToken(token.Value)
			if err != nil {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			// Make sure that the token is for resetting password purposes
			if claims.Audience != "reset-password" {
				return web.NewRedirectError("/login", http.StatusMovedPermanently)
			}

			// Add claims & token to the context so they can be retrieved later.
			ctx = context.WithValue(ctx, auth.KeyClaimsValue, claims)
			ctx = context.WithValue(ctx, auth.KeyTokenValue, token.Value)

			return after(ctx, w, r)
		}

		return h
	}

	return m
}
