package user

import (
	"context"
	"database/sql"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go-base-service/business/auth"
	"time"

	"golang.org/x/crypto/bcrypt"
)

var (
	// ErrNotFound is used when a specific User is requested but not exist.
	ErrNotFound = errors.New("not found")

	// ErrEmailAlreadyExists is used when trying to create or update an user
	// with an email that already exists in the db
	ErrEmailAlreadyExists = errors.New("email already exists")

	// ErrEmailNotFound is used when a user tries to authenticate
	// and the username(email) entered is not found in the db.
	ErrEmailNotFound = errors.New("email entered does not exist")

	// ErrPasswordIncorrect is used when a user tries to authenticate
	// but the password entered is incorrect
	ErrPasswordIncorrect = errors.New("password is incorrect")

	// ErrInvalidID occurs when an ID is not in a valid form.
	ErrInvalidID = errors.New("ID is not in its proper form")

	// ErrAuthenticationFailure occurs when a user attempts to authenticate but
	// something goes wrong.
	ErrAuthenticationFailure = errors.New("authentication failed")
)

// Create inserts a new User into the database.
func Create(ctx context.Context, db *sqlx.DB, nu NewUser, now time.Time) (User, error) {

	// Generate password hash to store it into the database instead of storing the password in plain.
	hash, err := bcrypt.GenerateFromPassword([]byte(nu.Password), bcrypt.DefaultCost)
	if err != nil {
		return User{}, errors.Wrapf(err, "generating password hash")
	}

	u := User{
		ID:           uuid.New().String(),
		Email:        nu.Email,
		PasswordHash: hash,
		IsGoogleUser: *nu.IsGoogleUser,
		DateCreated:  now.UTC(),
		DateUpdated:  now.UTC(),
	}

	const q = `INSERT INTO users (user_id, email, password_hash, is_google_user, date_created, date_updated) 
			VALUES (?, ?, ?, ?, ?, ?)`

	if _, err = db.ExecContext(ctx, q, u.ID, u.Email, u.PasswordHash, u.IsGoogleUser, u.DateCreated, u.DateUpdated); err != nil {
		// Checking if it's a duplicate key error as we have the email unique
		if e, ok := err.(*mysql.MySQLError); ok {
			if e.Number == 1062 {
				return User{}, ErrEmailAlreadyExists
			}
		}

		return User{}, errors.Wrapf(err, "inserting user into db")
	}

	return u, nil
}

// Update modified User data in the database.
func Update(ctx context.Context, db *sqlx.DB, id string, uu UpdateUser, now time.Time) error {

	// First get the user with the given id from the db.
	u, err := One(ctx, db, id)
	if err != nil {
		return err
	}

	// And then modify it with the changes from the UpdateUser.
	// We doing nil checks to see which fields have to be modified.
	if uu.Email != nil {
		u.Email = *uu.Email
	}
	if uu.Address != nil {
		u.Address = uu.Address
	}
	if uu.FullName != nil {
		u.FullName = uu.FullName
	}
	if uu.PhoneNumber != nil {
		u.PhoneNumber = uu.PhoneNumber
	}
	u.DateUpdated = now

	const q = `UPDATE users SET email = ?, address = ?, full_name = ?, phone_number = ?, date_updated = ? WHERE user_id = ?`

	if _, err = db.ExecContext(ctx, q, u.Email, u.Address, u.FullName, u.PhoneNumber, u.DateUpdated, id); err != nil {
		// Checking if it's a duplicate key error as we have the email unique
		if e, ok := err.(*mysql.MySQLError); ok {
			if e.Number == 1062 {
				return ErrEmailAlreadyExists
			}
		}

		return errors.Wrapf(err, "updating user with id %s into db", id)
	}

	return nil
}

// ChangePassword modified User password in the database.
func ChangePassword(ctx context.Context, db *sqlx.DB, id, password string, now time.Time) error {

	// First get the user with the given id from the db.
	u, err := One(ctx, db, id)
	if err != nil {
		return err
	}

	u.DateUpdated = now

	// Generate password hash to store it into the database instead of storing the password in plain.
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrapf(err, "generating password hash")
	}

	const q = `UPDATE users SET password_hash = ? WHERE user_id = ?`

	if _, err = db.ExecContext(ctx, q, hash, id); err != nil {
		return errors.Wrapf(err, "updating user with id %s into db", id)
	}

	return nil
}

// One is retrieving a user by its id
func One(ctx context.Context, db *sqlx.DB, id string) (User, error) {
	if _, err := uuid.Parse(id); err != nil {
		return User{}, ErrInvalidID
	}

	const q = `SELECT * from users WHERE user_id = ?`

	var u User
	if err := db.GetContext(ctx, &u, q, id); err != nil {
		if err == sql.ErrNoRows {
			return User{}, ErrNotFound
		}

		return User{}, errors.Wrapf(err, "selecting user with id %s from db", id)
	}

	return u, nil
}

// OneByEmailAndIsGoogle is retrieving a user by its email and based on where it was created from google api
func OneByEmailAndIsGoogle(ctx context.Context, db *sqlx.DB, email string, isGoogleUser bool) (User, error) {
	const q = `SELECT * FROM users WHERE email = ? AND is_google_user = ?`

	var u User
	if err := db.GetContext(ctx, &u, q, email, isGoogleUser); err != nil {
		if err == sql.ErrNoRows {
			return User{}, ErrEmailNotFound
		}

		return User{}, errors.Wrap(err, "selecting single user")
	}

	return u, nil
}

// OneByEmail is retrieving a user by its email
func OneByEmail(ctx context.Context, db *sqlx.DB, email string) (User, error) {
	const q = `SELECT * FROM users WHERE email = ?`

	var u User
	if err := db.GetContext(ctx, &u, q, email); err != nil {
		if err == sql.ErrNoRows {
			return User{}, ErrEmailNotFound
		}

		return User{}, errors.Wrap(err, "selecting single user")
	}

	return u, nil
}

// Authenticate finds a user by their email and verifies their password. On
// success it returns a Claims value representing this user. The claims can be
// used to generate a token for future authentication.
func Authenticate(ctx context.Context, db *sqlx.DB, now time.Time, email, password string, isGoogleUser bool) (auth.Claims, error) {
	u, err := OneByEmailAndIsGoogle(ctx, db, email, isGoogleUser)
	if err != nil {
		return auth.Claims{}, err
	}

	// Compare the provided password with the saved hash. Use the bcrypt
	// comparison function so it is cryptographically secure.
	if err := bcrypt.CompareHashAndPassword(u.PasswordHash, []byte(password)); err != nil {
		return auth.Claims{}, ErrPasswordIncorrect
	}

	// If we are this far the request is valid. Create some claims for the user
	// and generate their token.
	claims := auth.Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "auth-backend",
			Subject:   u.ID,
			Audience:  "go-frontend",
			ExpiresAt: now.Add(time.Minute * 30).Unix(),
			IssuedAt:  now.Unix(),
		},
	}

	return claims, nil
}
