package user_test

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"go-base-service/business/data/user"
	"go-base-service/business/tests"
	"golang.org/x/crypto/bcrypt"
	"testing"
	"time"
)

// userTests hold methods for each User subtest.
// It also makes passing dependencies to subtest simpler.
type userTests struct {
	db *sqlx.DB
}

// TestUser runs a series of subtests to exercise User behaviour from the DB level.
// All subtests share the same db and testing order matters because you can break a test
// that depends on another one.
func TestUser(t *testing.T) {
	db, teardown := tests.NewUnit(t)
	t.Cleanup(teardown)

	ut := userTests{db: db}

	t.Run("createUserEmailAlreadyExistFailed", ut.createUserEmailAlreadyExistFailed)
	t.Run("updateUserWithAndEmailThatAlreadyExistsFailed", ut.updateUserWithAndEmailThatAlreadyExistsFailed)
	t.Run("updateUserNotFoundFailed", ut.updateUserNotFoundFailed)
	t.Run("retrieveUserNotFoundFailed", ut.retrieveUserNotFoundFailed)
	t.Run("crudOperations", ut.crudOperations)
}

func (ut *userTests) createUserEmailAlreadyExistFailed(t *testing.T) {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	nu := user.NewUser{
		Email:           "user@example.com",
		Password:        "password",
		PasswordConfirm: "password",
		IsGoogleUser: user.BoolPointer(false),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("\tWhen creating a new user.")
		{
			_, err := user.Create(ctx, ut.db, nu, now)

			if err != nil {
				t.Fatalf("\t%s\tThe user should be created in the database: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tThe user should be created in the database.", tests.Success)

			t.Log("And then try to create the same user again")

			_, err = user.Create(ctx, ut.db, nu, now)

			if err == nil {
				t.Fatalf("\t%s\tThe user should NOT be created in the database, but it was.", tests.Failed)
			}
			t.Logf("\t%s\tThe user should NOT be created in the database.", tests.Success)

			if err != user.ErrEmailAlreadyExists {
				t.Fatalf("\t%s\tShould get the desired error. Expected err: %v, found %v", tests.Failed, user.ErrEmailAlreadyExists, err)
			}
			t.Logf("\t%s\tShould get the desired error.", tests.Success)
		}
	}
}

func (ut *userTests) updateUserWithAndEmailThatAlreadyExistsFailed(t *testing.T) {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	nu := user.NewUser{
		Email:           "test123@example.com",
		Password:        "password",
		PasswordConfirm: "password",
		IsGoogleUser: user.BoolPointer(false),
	}

	uu := user.UpdateUser{
		Email:       tests.StringPointer("user@example.com"),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When updating a user with an email that already exists for another user")
		{
			u, err := user.Create(ctx, ut.db, nu, now)
			if err != nil {
				t.Fatalf("\t%s\tThe user should be created in the database: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tThe user should be created in the database.", tests.Success)

			err = user.Update(ctx, ut.db, u.ID, uu, now)
			if err == nil {
				t.Fatalf("\t%s\tThe user should NOT be updated in the db.", tests.Failed)
			}
			t.Logf("\t%s\tThe user should NOT be updated in the db.", tests.Success)

			if err != user.ErrEmailAlreadyExists {
				t.Fatalf("\t%s\tShould get the desired error. Expected err: %v, found %v", tests.Failed, user.ErrEmailAlreadyExists, err)
			}
			t.Logf("\t%s\tShould get the desired error.", tests.Success)
		}
	}
}

func (ut *userTests) updateUserNotFoundFailed(t *testing.T) {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	uu := user.UpdateUser{
		Email:       tests.StringPointer("user@example.com"),
		FullName:    tests.StringPointer("Test Name"),
		Address:     tests.StringPointer("Test addr"),
		PhoneNumber: tests.StringPointer("40751322311"),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When updating a user that does not exist")
		{
			err := user.Update(ctx, ut.db, uuid.New().String(), uu, now)
			if err == nil {
				t.Fatalf("\t%s\tThe user should NOT be updated in the db.", tests.Failed)
			}
			t.Logf("\t%s\tThe user should NOT be updated in the db.", tests.Success)

			if err != user.ErrNotFound {
				t.Fatalf("\t%s\tShould get the desired error. Expected err: %v, found %v", tests.Failed, user.ErrNotFound, err)
			}
			t.Logf("\t%s\tShould get the desired error.", tests.Success)
		}
	}
}

func (ut *userTests) retrieveUserNotFoundFailed(t *testing.T) {
	ctx := tests.Context()

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When retrieving a user that does not exist")
		{
			_, err := user.One(ctx, ut.db, uuid.New().String())
			if err == nil {
				t.Fatalf("\t%s\tThe user should NOT be retrieved from the db.", tests.Failed)
			}
			t.Logf("\t%s\tThe user should NOT be retrieved from the db.", tests.Success)

			if err != user.ErrNotFound {
				t.Fatalf("\t%s\tShould get the desired error. Expected err: %v, found %v", tests.Failed, user.ErrNotFound, err)
			}
			t.Logf("\t%s\tShould get the desired error.", tests.Success)
		}
	}
}

func (ut *userTests) crudOperations(t *testing.T) {
	u := ut.createNewUserAndRetrieveSuccess(t)
	u = ut.updateUserAllFieldsAndRetrieveSuccess(t, u.ID)
	u = ut.updateUserOnlyEmailAndRetrieveSuccess(t, u)
	ut.retrieveUserSuccess(t, u)
}

func (ut *userTests) createNewUserAndRetrieveSuccess(t *testing.T) user.User {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	nu := user.NewUser{
		Email:           "testuser@example.com",
		Password:        "password",
		PasswordConfirm: "password",
		IsGoogleUser: user.BoolPointer(false),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("\tWhen creating a new user.")
		{
			u, err := user.Create(ctx, ut.db, nu, now)

			if err != nil {
				t.Fatalf("\t%s\tThe user should be created in the database: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tThe user should be created in the database.", tests.Success)

			u, err = user.One(ctx, ut.db, u.ID)
			if err != nil {
				t.Fatalf("\t%s\tAnd you should be able to retrieve by its id the new created user: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tAnd you should be able to retrieve by its id the new created user.", tests.Success)

			if u.Email != nu.Email {
				t.Fatalf("\t%s\tAnd the user should have the desired email. Found %s, Expected %s", tests.Failed, u.Email, nu.Email)
			}
			t.Logf("\t%s\tAnd the user should have the desired email.", tests.Success)

			if err := bcrypt.CompareHashAndPassword(u.PasswordHash, []byte(nu.Password)); err != nil {
				t.Fatalf("\t%s\tAnd the user should have the desired password hash.", tests.Failed)
			}
			t.Logf("\t%s\tAnd the user should have the desired password hash.", tests.Success)

			return u
		}
	}
}

func (ut *userTests) updateUserAllFieldsAndRetrieveSuccess(t *testing.T, id string) user.User {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	uu := user.UpdateUser{
		Email:       tests.StringPointer("test@example.com"),
		FullName:    tests.StringPointer("Test Name"),
		Address:     tests.StringPointer("Test addr"),
		PhoneNumber: tests.StringPointer("40751322311"),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When updating a new user")
		{
			if err := user.Update(ctx, ut.db, id, uu, now); err != nil {
				t.Fatalf("\t%s\tThe user should be updated in the db: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tThe user should be updated in the db.", tests.Success)

			u, err := user.One(ctx, ut.db, id)
			if err != nil {
				t.Fatalf("\t%s\tAnd you should be able to retrieve by its id the new updated user: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tAnd you should be able to retrieve by its id the new updated user.", tests.Success)

			if u.Email != *uu.Email {
				t.Fatalf("\t%s\tAnd the user should have the desired email. Found %s, Expected %s", tests.Failed, u.Email, *uu.Email)
			}
			t.Logf("\t%s\tAnd the user should have the desired email.", tests.Success)

			if *u.FullName != *uu.FullName {
				t.Fatalf("\t%s\tAnd the user should have the desired full name. Found %s, Expected %s", tests.Failed, *u.FullName, *uu.FullName)
			}
			t.Logf("\t%s\tAnd the user should have the desired full name.", tests.Success)

			if *u.Address != *uu.Address {
				t.Fatalf("\t%s\tAnd the user should have the desired address. Found %s, Expected %s", tests.Failed, *u.Address, *uu.Address)
			}
			t.Logf("\t%s\tAnd the user should have the desired address .", tests.Success)

			if *u.PhoneNumber != *uu.PhoneNumber {
				t.Fatalf("\t%s\tAnd the user should have the desired phone number. Found %s, Expected %s", tests.Failed, *u.PhoneNumber, *uu.PhoneNumber)
			}
			t.Logf("\t%s\tAnd the user should have the desired phone number.", tests.Success)

			return u
		}
	}
}

func (ut *userTests) updateUserOnlyEmailAndRetrieveSuccess(t *testing.T, u user.User) user.User {
	ctx := tests.Context()
	now := time.Date(1992, time.July, 24, 0, 0, 0, 0, time.UTC)

	uu := user.UpdateUser{
		Email: tests.StringPointer("test2@example.com"),
	}

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When updating a new user")
		{
			if err := user.Update(ctx, ut.db, u.ID, uu, now); err != nil {
				t.Fatalf("\t%s\tThe user should be updated in the db: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tThe user should be updated in the db.", tests.Success)

			ru, err := user.One(ctx, ut.db, u.ID)
			if err != nil {
				t.Fatalf("\t%s\tAnd you should be able to retrieve by its id the new updated user: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tAnd you should be able to retrieve by its id the new updated user.", tests.Success)

			if ru.Email != *uu.Email {
				t.Fatalf("\t%s\tAnd the user should have the desired email. Found %s, Expected %s", tests.Failed, ru.Email, *uu.Email)
			}
			t.Logf("\t%s\tAnd the user should have the desired email.", tests.Success)

			if *ru.FullName != *u.FullName {
				t.Fatalf("\t%s\tAnd the user should have full name unchanged. Found %s, Expected %s", tests.Failed, *ru.FullName, *u.FullName)
			}
			t.Logf("\t%s\tAnd the user should have full name unchanged.", tests.Success)

			if *ru.Address != *u.Address {
				t.Fatalf("\t%s\tAnd the user should have address unchanged. Found %s, Expected %s", tests.Failed, *ru.Address, *u.Address)
			}
			t.Logf("\t%s\tAnd the user should have address unchanged .", tests.Success)

			if *ru.PhoneNumber != *u.PhoneNumber {
				t.Fatalf("\t%s\tAnd the user should have phone number unchanged. Found %s, Expected %s", tests.Failed, *ru.PhoneNumber, *u.PhoneNumber)
			}
			t.Logf("\t%s\tAnd the user should have phone number unchanged.", tests.Success)

			return ru
		}
	}
}

func (ut *userTests) retrieveUserSuccess(t *testing.T, u user.User) user.User {
	ctx := tests.Context()

	t.Log("Given the need to work with User records inside a db.")
	{
		t.Log("When retrieving a new user")
		{
			ru, err := user.One(ctx, ut.db, u.ID)
			if err != nil {
				t.Fatalf("\t%s\tYou should be able to retrieve by its id the new updated user: %v", tests.Failed, err)
			}
			t.Logf("\t%s\tYou should be able to retrieve by its id the new updated user.", tests.Success)

			if ru.Email != u.Email {
				t.Fatalf("\t%s\tAnd the user should have the desired email. Found %s, Expected %s", tests.Failed, ru.Email, u.Email)
			}
			t.Logf("\t%s\tAnd the user should have the desired email.", tests.Success)

			if *ru.FullName != *u.FullName {
				t.Fatalf("\t%s\tAnd the user should have the desired full name. Found %s, Expected %s", tests.Failed, *ru.FullName, *u.FullName)
			}
			t.Logf("\t%s\tAnd the user should have the desired full name.", tests.Success)

			if *ru.Address != *u.Address {
				t.Fatalf("\t%s\tAnd the user should have the desired address. Found %s, Expected %s", tests.Failed, *ru.Address, *u.Address)
			}
			t.Logf("\t%s\tAnd the user should have the desired address .", tests.Success)

			if *ru.PhoneNumber != *u.PhoneNumber {
				t.Fatalf("\t%s\tAnd the user should have the desired phone number. Found %s, Expected %s", tests.Failed, *ru.PhoneNumber, *u.PhoneNumber)
			}
			t.Logf("\t%s\tAnd the user should have the desired phone number.", tests.Success)

			return ru
		}
	}
}
