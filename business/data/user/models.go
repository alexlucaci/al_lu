package user

import (
	"time"
)

// User holds information about a user.
// Fields that can have NULL values in the db will be as pointer fields for unmarshalling to work.
type User struct {
	ID           string    `db:"user_id" json:"id"`
	Email        string    `db:"email" json:"email"`
	PasswordHash []byte    `db:"password_hash" json:"-"`
	FullName     *string   `db:"full_name" json:"full_name"`
	Address      *string   `db:"address" json:"address"`
	PhoneNumber  *string   `db:"phone_number" json:"phone_number"`
	IsGoogleUser bool      `db:"is_google_user" json:"is_google_user"`
	DateCreated  time.Time `db:"date_created" json:"date_created"`
	DateUpdated  time.Time `db:"date_updated" json:"date_updated"`
}

// NewUser contains information needed to create a new User.
type NewUser struct {
	Email           string `json:"email" validate:"required"`
	Password        string `json:"password" validate:"required"`
	PasswordConfirm string `json:"password_confirm" validate:"required,eqfield=Password"`
	IsGoogleUser    *bool   `json:"is_google_user" validate:"required"`
}

// UpdateUser defines what information may be provided to modify an existing
// User. All fields are optional so clients can send only the fields they want
// changed. It uses pointer fields so we can differentiate between a field that
// was not provided and a field that was provided as explicitly blank.
type UpdateUser struct {
	Email       *string `json:"email"`
	FullName    *string `json:"full_name"`
	Address     *string `json:"address"`
	PhoneNumber *string `son:"phone_number"`
}

func BoolPointer(b bool) *bool {
	return &b
}
