package schema

import "github.com/jmoiron/sqlx"

// Seed runs the set of seed-data queries against db. The queries are ran in a
// transaction and rolled back if any fail.
func Seed(db *sqlx.DB) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(seeds); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}
		return err
	}
	return tx.Commit()
}

// SQL query for adding user with email: `user@example.com` and password: `gophers`
const seeds = `INSERT INTO users (user_id, email, full_name, address, phone_number, password_hash, is_google_user, date_created, date_updated) VALUES 
                ('2b51b48b-f0ac-4a29-bafd-c4c6a5addd14', 'user@example.com', 'Alex Lucaci', 'Empty address', '40762123444', '$2a$10$1ggfMVZV6Js0ybvJufLRUOWHS5f6KneuP0XwwHpJ8L8ipdry9f2/a', FALSE, NOW(), NOW()),
				('ca6b1970-668d-42d3-928e-fd98294c8ef3', 'alex.lucaci92@gmail.com', 'Alex Lucaci', 'Empty address', '40762123444', '$2a$10$zQvJXXh8YBYRM8lgAXdEDOYwX51ekrsGfJTyr/KQ27ozEKWEnixBq', TRUE, NOW(), NOW())
				ON DUPLICATE KEY UPDATE user_id=user_id;`
