package schema

import (
	"github.com/dimiro1/darwin"
	"github.com/jmoiron/sqlx"
)

// Migrate attempts to bring the schema for db up to date with the migrations
// defined in this package.
func Migrate(db *sqlx.DB) error {
	driver := darwin.NewGenericDriver(db.DB, darwin.MySQLDialect{})
	d := darwin.New(driver, migrations, nil)
	return d.Migrate()
}

// migrations contains the queries needed to construct the database schema.
// Entries should never be removed from this slice once they have been ran in
// production.
var migrations = []darwin.Migration{
	{
		Version:     1,
		Description: "Add users",
		Script: `
		CREATE TABLE users (
			user_id BINARY(36) NOT NULL,
			email VARCHAR(255) UNIQUE NOT NULL,
			full_name VARCHAR(255),
			address VARCHAR(255),
			phone_number VARCHAR(255),
			password_hash VARCHAR(255) NOT NULL,
			is_google_user BOOLEAN NOT NULL,
			date_created TIMESTAMP,
			date_updated TIMESTAMP,
	
			PRIMARY KEY (user_id)
		) DEFAULT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';`,
	},
}
